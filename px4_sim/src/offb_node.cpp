#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <mavros_msgs/CommandBool.h>
#include <mavros_msgs/SetMode.h>
#include <mavros_msgs/State.h>
#include <chrono>
#include <iostream>
#include <string>
#include <sstream>
#include <sensor_msgs/Range.h>




mavros_msgs::State current_state;
void state_cb(const mavros_msgs::State::ConstPtr& msg) {
    current_state = *msg;
}

sensor_msgs::Range current_range;
void range_cb(const sensor_msgs::Range::ConstPtr& msg) {
    current_range = *msg;
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "offb_main");
    ros::NodeHandle nh;

    ros::Subscriber state_sub = nh.subscribe<mavros_msgs::State>
                                ("mavros/state", 10, state_cb);
    ros::Publisher local_pos_pub = nh.advertise<geometry_msgs::PoseStamped>
                                   ("mavros/setpoint_position/local", 10);
    ros::ServiceClient arming_client = nh.serviceClient<mavros_msgs::CommandBool>
                                       ("mavros/cmd/arming");
    ros::Subscriber range_sub = nh.subscribe<sensor_msgs::Range>
            ("mavros/px4flow/ground_distance", 1, range_cb);
    ros::ServiceClient set_mode_client = nh.serviceClient<mavros_msgs::SetMode>
                                         ("mavros/set_mode");

    // The setpoint publishing rate MUST be faster than 2Hz.
    ros::Rate rate(20.0);

    // Wait for FCU connection.
    while (ros::ok() && current_state.connected) {
        ros::spinOnce();
        rate.sleep();
    }

    std::chrono::time_point<std::chrono::high_resolution_clock> point_start, point_now;
    point_now = std::chrono::high_resolution_clock::now();
    geometry_msgs::PoseStamped pose;
    pose.pose.position.x = 0;
    pose.pose.position.y = 0;
   // pose.pose.position.z = 2;

    mavros_msgs::SetMode offb_set_mode;
    sensor_msgs::Range range_msg;
    offb_set_mode.request.custom_mode = "OFFBOARD";

    mavros_msgs::CommandBool arm_cmd;
    arm_cmd.request.value = true;

    ros::Time last_request(0);
    auto end = std::chrono::seconds(20);

    bool is_fly = false;


    while (ros::ok()) {
	    if (!current_state.armed && (ros::Time::now() - last_request > ros::Duration(5.0))) {
	        if(arming_client.call(arm_cmd) && arm_cmd.response.success ){
	            ROS_INFO("Offboard enabled");
            }
            last_request = ros::Time::now();
	    } else if (current_state.mode == "AUTO.LAND" && (ros::Time::now() - last_request > ros::Duration(5.0))) {
	        if(set_mode_client.call(offb_set_mode) && offb_set_mode.response.mode_sent) {
                ROS_INFO("I believe I can fly");
            }
            last_request = ros::Time::now();
        }
      //  ROS_INFO("range %f", current_range.range);
      if(current_range.range < 2.0) {
          pose.pose.position.z += 0.02;
      } else if (current_range.range > 2.2) {
          pose.pose.position.z -= 0.02;
      }
	    if (current_range.range >= 2.0 && is_fly==false) {
            ROS_INFO("i'm here %f", current_range.range);
	        is_fly = true;
	        point_start = std::chrono::high_resolution_clock::now();
	    }
        auto timer = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::high_resolution_clock::now() - point_start);
	    if(timer >= end && is_fly) {
            offb_set_mode.request.custom_mode = "AUTO.LAND";
            if(set_mode_client.call(offb_set_mode) && offb_set_mode.response.mode_sent) {
                ROS_INFO("AUTO.LAND");
            }
	    }

	    /*if (!current_state.armed && (ros::Time::now() - last_request > ros::Duration(5.0))) {
	        if( arming_client.call(arm_cmd) && arm_cmd.response.success) {
	            ROS_INFO("Vehicle armed");
	            timer = ros::Time::now();
	            if(ros::Time::now() - last_request > ros::Duration(7.0)) {
	                offb_set_mode.request.custom_mode = "AUTO.LAND";
	                set_mode_client.call(offb_set_mode);
	                arm_cmd.request.value = false;
	                arming_client.call(arm_cmd);
	                ROS_INFO("AUTO.LAND");
	             }
	        }
	        last_request = ros::Time::now();
	    }*/

        local_pos_pub.publish(pose);

        ros::spinOnce();
        rate.sleep();
    }

    return 0;
}
